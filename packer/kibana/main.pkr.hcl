locals {
  canonical_owner_id = "099720109477"
  image_description  = "Kibana v${var.version}"
  image_name         = "kibana-${replace(var.version, ".", "-")}"
  region             = "eu-west-3"
}

source amazon-ebs "aws" {
  ami_description      = local.image_description
  ami_name             = local.image_name
  instance_type        = "t2.small"
  launch_block_device_mappings {
    delete_on_termination = true
    device_name           = "/dev/sda1"
    iops                  = var.disk_iops
    throughput            = var.disk_throughput
    volume_size           = var.disk_size
    volume_type           = "gp3"
  }
  region = local.region
  source_ami_filter {
    filters = {
      name                = "ubuntu/images/*ubuntu-focal-20.04-amd64-server-*"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = [local.canonical_owner_id]
  }
  ssh_username = "ubuntu"
  tags = {
    CreatedBy = "packer"
  }
}

build {
  sources = ["source.amazon-ebs.aws"]

  provisioner "shell" {
    inline = [
      "wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -",
      "sudo apt-get install apt-transport-https",
      "echo \"deb https://artifacts.elastic.co/packages/7.x/apt stable main\" | sudo tee /etc/apt/sources.list.d/elastic-7.x.list",
      "sudo apt-get update && sudo apt-get install kibana=\"${var.version}\"",
    ]
    inline_shebang = "/bin/bash -e"
  }
}
