variable "disk_iops" {
  description = "Disk IOPS"
  default     = 3000
  type        = number
}

variable "disk_size" {
  description = "Disk size (GB)"
  default     = 10
  type        = number
}

variable "disk_throughput" {
  description = "Disk throughput (MiB/s)"
  default     = 125
  type        = number
}

variable "version" {
  description = "Image version"
  default     = "7.10.2"
  type        = string
}
