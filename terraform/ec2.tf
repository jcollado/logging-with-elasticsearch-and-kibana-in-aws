data "aws_ami" "ubuntu" {
  most_recent = true
  owners      = [local.canonical_owner_id]
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_security_group" "http_and_ssh" {
  name        = "HttpAndSsh"
  description = "Allow HTTP and SSH traffic"

  ingress {
    description = "Allow HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Allow SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "Allow all egress traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = local.tags
}

resource "aws_instance" "wordpress" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t3.micro"
  key_name               = "liveProject"
  tags                   = merge(local.tags, { Name = "wordpress" })
  vpc_security_group_ids = [aws_security_group.http_and_ssh.id]

  provisioner "local-exec" {
    command     = <<-EOF
      aws ec2 wait instance-running --instance-ids ${self.id}
      sleep 30
      ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook \
        --user ubuntu \
        --key-file ~/.ssh/liveProject.pem \
        --inventory '${self.public_ip},' \
        --extra-vars 'elasticsearch_host=${aws_instance.node[0].private_ip}' \
        wordpress.yml
    EOF
    working_dir = "${path.module}/../ansible"
  }
}


resource "aws_instance" "microservices" {
  ami                    = data.aws_ami.ubuntu.id
  key_name               = "liveProject"
  instance_type          = "t3.micro"
  tags                   = merge(local.tags, { Name = "microservices" })
  vpc_security_group_ids = [aws_security_group.http_and_ssh.id]

  provisioner "local-exec" {
    command     = <<-EOF
      aws ec2 wait instance-running --instance-ids ${self.id}
      sleep 30
      ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook \
        --user ubuntu \
        --key-file ~/.ssh/liveProject.pem \
        --inventory '${self.public_ip},' \
        --extra-vars 'elasticsearch_host=${aws_instance.node[0].private_ip}' \
        microservices.yml
    EOF
    working_dir = "${path.module}/../ansible"
  }
}
