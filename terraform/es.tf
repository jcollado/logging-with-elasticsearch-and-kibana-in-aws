locals {
  account_id = data.aws_caller_identity.current.account_id
  ebs_size   = 10
  ebs_type   = "gp2"
}

data "aws_caller_identity" "current" {}

data "aws_ami" "elasticsearch" {
  most_recent = true
  owners      = [local.account_id]
  filter {
    name   = "name"
    values = ["elasticsearch-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_security_group" "elasticsearch" {
  name        = "Elasticsearch"
  description = "Allow Elasticsearch traffic"
}

resource "aws_security_group_rule" "api" {
  security_group_id = aws_security_group.elasticsearch.id

  type                     = "ingress"
  from_port                = 9200
  to_port                  = 9200
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.elasticsearch.id
}

resource "aws_security_group_rule" "cluster" {
  security_group_id = aws_security_group.elasticsearch.id

  type                     = "ingress"
  from_port                = 9300
  to_port                  = 9300
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.elasticsearch.id
}

resource "aws_security_group_rule" "ssh" {
  security_group_id = aws_security_group.elasticsearch.id

  type        = "ingress"
  from_port   = 22
  to_port     = 22
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "egress" {
  security_group_id = aws_security_group.elasticsearch.id

  type        = "egress"
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_iam_role" "elasticsearch" {
  name = "elasticsearch"

  assume_role_policy = jsonencode(
    {
      Statement = [
        {
          Action = "sts:AssumeRole"
          Effect = "Allow"
          Principal = {
            Service = "ec2.amazonaws.com"
          }
        }
      ]
      Version = "2012-10-17"
    }
  )
}

resource "aws_iam_role_policy" "elasticsearch" {
  name = "DiscoveryEc2"
  role = aws_iam_role.elasticsearch.id

  policy = jsonencode(
    {
      Statement = [
        {
          Action   = "ec2:DescribeInstances"
          Effect   = "Allow"
          Resource = "*"
        }
      ]
      Version = "2012-10-17"
    }
  )
}

resource "aws_iam_instance_profile" "elasticsearch" {
  name = "elasticsearch"
  role = aws_iam_role.elasticsearch.name
}

resource "aws_instance" "node" {
  count = 3

  ami                  = data.aws_ami.elasticsearch.id
  iam_instance_profile = aws_iam_instance_profile.elasticsearch.name
  instance_type        = "t3.small"
  key_name             = "liveProject"
  tags = merge(
    local.tags,
    {
      Name    = "${var.cluster_name}-${count.index + 1}"
      cluster = "elasticsearch-cluster"
    },
  )
  vpc_security_group_ids = [aws_security_group.elasticsearch.id]

  provisioner "local-exec" {
    command     = <<-EOF
      aws ec2 wait instance-running --instance-ids ${self.id}
      sleep 30
      ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook \
        --user ubuntu \
        --key-file ~/.ssh/liveProject.pem \
        --inventory '${self.public_ip},' \
        --extra-vars 'cluster_name=${var.cluster_name}' \
        --extra-vars 'initial_master_node=${var.cluster_name}-1' \
        --extra-vars 'node_name=${var.cluster_name}-${count.index + 1}' \
        --extra-vars 'region=${local.region}' \
        elasticsearch.yml
    EOF
    working_dir = "${path.module}/../ansible"
  }
}
