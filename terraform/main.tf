terraform {
  required_version = ">=0.14.4"
}

provider "aws" {
  region = "eu-west-3"
}

locals {
  canonical_owner_id = "099720109477"
  region             = "eu-west-3"
  tags = {
    CreatedBy = "terraform"
  }
}
