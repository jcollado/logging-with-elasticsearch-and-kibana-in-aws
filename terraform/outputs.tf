output "wordpress_url" {
  description = "Wordpress URL"
  value       = "http://${aws_instance.wordpress.public_ip}"
}
