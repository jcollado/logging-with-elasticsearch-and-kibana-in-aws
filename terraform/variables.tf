variable "cluster_name" {
  description = "Elasticsearch cluster name"
  type        = string
  default     = "my-elasticsearch-cluster"
}

variable "node_count" {
  description = "Number of nodes in the Elasticsearch cluster"
  type        = number
  default     = 3
}
