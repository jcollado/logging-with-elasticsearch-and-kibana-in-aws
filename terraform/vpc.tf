resource "aws_default_vpc" "default" {
  tags = local.tags
}

resource "aws_default_subnet" "subnet" {
  for_each          = toset(["a", "b", "c"])
  availability_zone = "${local.region}${each.value}"
}
